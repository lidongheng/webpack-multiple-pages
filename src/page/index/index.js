
require('./index.css')
require('page/common/header/index')
require('page/common/footer/index')

var _tools = require('utils/tools')
var templateHTML = require('./index.string')

var index = {
  // 此数据可以通过调用service中的服务请求加载回来进行渲染
  data: {
    list: [
      {username: '战三', age: 25},
      {username: '李四', age: 24},
      {username: '王五', age: 26}
    ]
  },
  init: function(){
    this.bindEvent()
    this.renderList()
  },
  bindEvent: function(){
    $(".main").click(function(){
      alert("我被点击了")
    })
  },
  renderList: function(){
    var contentHTML = _tools.renderHTML(templateHTML, {
      list: this.data.list
    })
    $("#list").html(contentHTML)
  }
}

$(function(){
  index.init()
})
