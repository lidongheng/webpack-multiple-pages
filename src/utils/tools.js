var Hogan = require("hogan");

// 判断是否是开发环境，这里使用的是Webpack.DefinePlugin定义的环境变量
var debug = process.env.NODE_ENV === "dev"

var tools = {
  // 网络请求方法
  request: function (params) {
    var _this = this;
    // 使用了webpack-dev-server做了代理，以api开头的
    if (debug) {
      params.url = "/api" + params.url;
    } else {
      params.url = process.env.BASE_API + params.url;
    }
    $.ajax({
      url: params.url || "",
      data: params.data || "",
      type: params.type || "GET",
      dataType: params.dataType || "json",
      success: function (res) {
        // 请求成功，具体情况具体分析
        if (0 === res.code) {
          typeof params.success === 'function' && params.success(res.data);
        } else if (0 !== res.code) {
          typeof params.error === 'function' && params.error(res.msg);
        }
      },
      error: function(err) {
        typeof params.error === "function" && params.error(err);
      }
    })
  },
  /**
   * 作用：根据模板和data渲染页面
   * template: html模板名称
   * data：需要渲染的数据
   */
  renderHTML: function (htmlTemplate,data) {
    return Hogan.compile(htmlTemplate).render(data);
  },
  // 获取url上的查询参数值
  // http://www.baidu.com?keyword=手机&page=1&orderby=default
  getUrlParam: function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"),
        result = window.location.search.substr(1).match(reg);
    return result ? decodeURIComponent(result[2]) : null;
  },
  // path格式：'/user/login.do'
  // 本地开发和打包的地址，process.env.BASE_API 是在prod.env.js中配置的
  getServerUrl: function (path) {
    if (debug) {
      return window.location.origin + path;
    } else {
      return process.env.BASE_API + path;
    }
  },
  // 服务器返回的是图片的UUID
  getImgUrl: function (uuid) {
    var path = '';
    if (debug) {
      path = 'api/common/image/'
    } else {
      path = 'common/image/'
    }
    return uuid ? path + uuid : '';
  },
  // 字段的验证，支持非空、手机、邮箱的判断
  validate : function(value, type){
    var value = $.trim(value);
    // 非空验证
    if('require' === type){
        return !!value;
    }
    // 手机号验证
    if('phone' === type){
        return /^1\d{10}$/.test(value);
    }
    // 邮箱格式验证
    if('email' === type){
        return /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/.test(value);
    }
  },
  getObjectURL: function(file) {
    var url = null;
    if (window.createObjectURL != undefined) { // basic
      url = window.createObjectURL(file);
    } else if (window.URL != undefined) { // mozilla(firefox)
      url = window.URL.createObjectURL(file);
    } else if (window.webkitURL != undefined) { // webkit or chrome
      url = window.webkitURL.createObjectURL(file);
    }
    return url;
  }
}

module.exports = tools;
