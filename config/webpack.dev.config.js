const path = require("path");
// 引入基础配置文件
const webpackBase = require("./webpack.base.config");
// 引入 webpack-merge 插件
const webpackMerge = require("webpack-merge");
// 引入webpack
const Webpack = require("webpack");
// 配置
const config = require("./config");
// 友好的错误提示
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const utils = require('./utils')

// 合并配置文件
module.exports = webpackMerge(webpackBase,{
  // 配置 webpack-dev-server
  devServer:{
    // 项目根目录
    contentBase: path.join(__dirname, '..', 'dist'),
    clientLogLevel: 'warning',
    hot: true, // 启用 webpack 的模块热替换特性，此特性需要加载plugins:webpack.HotModuleReplacementPlugin
    compress: true, // 开启gzip压缩
    host: config.dev.host,
    progress: true, // 启动的过程也要显示出进度
    port: config.dev.port,
    open: config.dev.autoOpenBrowser, // 不自动打开浏览器
    publicPath: config.dev.assetsPublicPath,
    quiet: true, // necessary for FriendlyErrorsPlugin
    // 错误、警告展示设置
    overlay: config.dev.errorOverlay ? 
      { errors: true, warnings: false } : false,
    watchOptions: {
      poll: config.dev.poll
    },
    // 代理
    proxy: config.dev.proxyTable
  },
  plugins: [
    // DefinePlugin 允许创建一个在编译时可以配置的全局常量，可以在JS文件中根据这些定义的变量来定义不同的行为
    new Webpack.DefinePlugin({
      'process.env': require("./dev.env")
    }),
    // 热加载需要配置的插件
    new Webpack.HotModuleReplacementPlugin(),
    // 配合quite为true
    new FriendlyErrorsPlugin({
      compilationSuccessInfo: {
        messages: [`Your application is running here: http://${config.dev.host}:${config.dev.port}`],
      },
      onErrors: config.dev.notifyOnErrors
        ? utils.createNotifierCallback()
        : undefined
    })
  ]
})