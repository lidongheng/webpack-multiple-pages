const path = require("path");
// 引入插件，自动生成HTML文件的插件
const HTMLWebpackPlugin = require("html-webpack-plugin");
// 抽取 css 到单独的文件
const ExtractTextPlugin = require("extract-text-webpack-plugin");
// 引入webpack
const Webpack = require("webpack");
// 基本配置
const config = require("./config");
// 工具类
const utils = require("./utils");

// 计算HtmlWebpackPlugin的参数
// template是html文件的名称，title是html文件中的title值
function getHtmlWebpackPluginParams(template,title){
  // 生成的HTML文件放到/dist/view目录下
  return {
    filename: `${template}.html`, // 生成的文件放在/dist目录下
    template: utils.resolve(`src/view/${template}.html`), // 模板的位置
    title: title, // 设置HTML文件中的title值
    inject: true, // 插入的script标签放在body后面
    hash: true, // 产生hash值
    chunks: [template, 'common'], // script标签中插入的文件为对应html文件的js以及公用的common
    favicon: utils.resolve('favicon.ico') // 自动为html插入favicon.ico
  }
}

var configs = {
  // 多页应用，所以在入口处设置多个入口的JS文件
  entry: {
    "common": ["./src/page/common/index.js"], // 公用的JS文件
    "index": ["./src/page/index/index.js"] // 首页页面的JS文件
  },
  // 上面打包出来的 js 文件放到 "dist/js" 文件夹下
  output:{
    filename: 'js/[name].[chunkhash].js', // 【chunkhash】改变内容的时候，hash值就会改变，到时候会强制浏览器获取新的文件
    publicPath: process.env.NODE_ENV === 'prod'
      ? config.prod.assetsPublicPath
      : config.dev.assetsPublicPath,
    path: utils.resolve('dist') // 打包的文件放到/dist下
  },
  externals : {
    'jquery' : 'window.jQuery' // 在页面中通过script引入JQ，在page目录下对应的JS文件可以直接使用$符号
  },
  // 加载器
  module: {
    rules: [
      {
        // 对 css 后缀名进行处理
        test:/\.css$/,
        // 抽取 css 文件到单独的文件夹
        use: ExtractTextPlugin.extract({
          fallback: "style-loader", // 编译后用什么style-loader来提取css文件
          use: [
            {
              loader:"css-loader",
              options:{
                minimize:true, // 开启 css 压缩
              }
            },
            {
              loader:"postcss-loader",
            }
          ]
        })
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 100,
          name: path.posix.join('image/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 100,
          name: path.posix.join('media/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 100,
          name: path.posix.join('fonts/[name].[hash:7].[ext]')
        }
      },
      // hogen.js加载的html模板
      { 
        test: /\.string$/,
        loader: 'html-loader'
      }
    ],
  },
  // 别名
  resolve : {
    alias : {
      '@'             : utils.resolve('src'),
      node_modules    : utils.resolve('node_modules'),
      utils           : utils.resolve('src/utils'),
      page            : utils.resolve('src/page'),
      service         : utils.resolve('src/service'),
      image           : utils.resolve('src/image')
    }
  },
  plugins:[
    // 抽取公用的JS文件，打包到/dist/js/common.js文件中，也会把common 的chunk打包到/dist/js/common.js中
    new Webpack.optimize.CommonsChunkPlugin({
      name: "common", // 入口中的common chunk
      filename: "js/common.[hash].js"
    }),
    // 将 css 抽取到/dist/css/xxx.css
    new ExtractTextPlugin("css/[name].css"),
    // 自动生成 HTML 插件
    new HTMLWebpackPlugin(getHtmlWebpackPluginParams('index','首页'))
  ]
}

module.exports = configs;