'use strict'
/**
 * 生产环境使用相对应的URL接口，一定要有一个双引号""
 */
module.exports = {
  NODE_ENV: '"prod"',
  BASE_API: '"http://app.qwlpt.com"'
}
