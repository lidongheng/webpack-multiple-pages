'use strict'
const path = require('path')
const packageConfig = require('../package.json')

// 本地开发启动时候的提示
exports.createNotifierCallback = function(){
  const notifier = require('node-notifier')

  return (severity, errors) => {
    if (severity !== 'error') return

    const error = errors[0]
    const filename = error.file && error.file.split('!').pop()

    notifier.notify({
      title: packageConfig.name,
      message: severity + ': ' + error.name,
      subtitle: filename || '',
      icon: path.join(__dirname, 'logo.png')
    })
  }
}

// 解析路径，当前config文件的上一级路径
exports.resolve = function(dir){
  return path.join(__dirname, '..', dir)
}