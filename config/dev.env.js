'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

/**
 * 本地开发环境使用代理访问其他同事的本地环境，所以不设置BASE_URL,一定要有一个双引号""
 */
module.exports = merge(prodEnv, {
  NODE_ENV: '"dev"'
})
