// 引入基础配置
const webpackBase = require("./webpack.base.config");
// 引入 webpack-merge 插件
const webpackMerge = require("webpack-merge");
// 引入 webpack
const webpack = require("webpack");
// 基本配置
const config = require("./config");
// 清理 dist 文件夹
const CleanWebpackPlugin = require("clean-webpack-plugin")
// 工具类
const utils = require("./utils");

// 合并配置文件
module.exports = webpackMerge(webpackBase,{
  plugins:[
    // DefinePlugin 允许创建一个在编译时可以配置的全局常量，可以在JS文件中根据这些定义的变量来定义不同的行为
    new webpack.DefinePlugin({
      'process.env': require("./prod.env")
    }),
    // 自动清理 dist 文件夹
    new CleanWebpackPlugin(["dist"],{
      root: utils.resolve('/'),
      verbose: true,
      dry: false
    }),
    // 代码压缩
    new webpack.optimize.UglifyJsPlugin({
      uglifyOptions: {
        output: {
          comments: false, // 打包的东西去掉注释
        },
        compress: {
          warnings: false
        }
      },
      sourceMap: config.prod.productionSourceMap, // 开启 sourceMap
      parallel: true
    })
  ]
});