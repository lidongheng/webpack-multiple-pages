module.exports = {
  dev: {
    assetsPublicPath: '/',
    proxyTable: {
      '/api': {
        target: 'http://192.168.9.19:8080',
        changeOrigin: true, //改变源
        pathRewrite: {
          '^/api': ''
        }
      }
    },
    host: '0.0.0.0', // 写成0.0.0.0手机可以通过局域网访问，localhost无法使用手机访问的
    port: 8888,
    notifyOnErrors: true,
    autoOpenBrowser: false, // 是否自动打开浏览器
    errorOverlay: true, // 编译错误的时候，在浏览器显示mask
    poll: false,
  },
  prod: {
    assetsPublicPath: '/', // 线上静态资源的地址的根路径，此处可以根据实际情况修改
    productionSourceMap: true // 是否生成sourceMap
  }
}