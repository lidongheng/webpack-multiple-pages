# webpack-multiple-pages

#### 项目介绍
一个基于webpack的多页demo

#### 软件架构
软件架构说明


#### 安装教程

1. npm install或者cnpm install
2. 开发环境：npm run dev
3. 生成环境：npm run build
4. 打包完成之后，可以运行npm run server查看静态资源文件

#### 使用说明

1. src/image    存放图片
2. src/page     存放对应HTML模板使用的JS/CSS代码
3. src/service  存放项目和服务器交互的ajax代码
4. src/utils    存放工具类
5. src/view     存放HTML页面
6. 请求服务器的js调用逻辑：page --> service --> utils --> ajax
7. hogan.js     可以在page中js文件中读取xxx.string文件的html模板用于渲染，调用tools文件中的renderHTML方法进行处理，
                把渲染之后的html使用$("xxx").html(xxx)挂载到页面上
8. 打包生成的目录结构：
   /dist
    /css
    /image
    /js
    favicon.ico
    /xxx.html  
9. 并没有配置babel-loader，只支持ES5的写法，需要的可以自己扩展               

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)